<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use Session;

class GiphyController extends Controller
{

    /**
     * show function
     * Queries api to fetch trending giphys and passes data to the view.
     * 
     * @return view
     */
    public function show()
    {
        try {
            $client = new \GuzzleHttp\Client();
            $service_url = 'http://api.giphy.com/v1/gifs/trending?api_key=' . 'ME169lHlroe4NDoWs6rpe6kM7AyW6n6l' . '&limit=12';
            $response = $client->request('GET', $service_url)->getBody();
            $responseBody = json_decode($response->getContents());
            $responseData = (array) $responseBody->data;

            //storing data in session
            session()->put('data', $responseData);
            
        } catch (ClientException $e) {
            if ($e->hasResponse()) {
                dd('ERROR');
            }
        }
        return View('/welcome')->with(array('response' => $responseData));
    }
}
