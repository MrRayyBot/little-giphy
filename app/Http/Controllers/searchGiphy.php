<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Session;
use Rememberable;


class searchGiphy extends Controller
{
    /**
     * Queries Api, and redirects to /searchResults passing the data.
     * 
     * Caches Data and fetches from cache if available.
     * 
     * @return redirect
     */
    public function search()
    {
        try {
            $query = $_POST['search'];
            $client = new \GuzzleHttp\Client();
            $service_url = 'http://api.giphy.com/v1/gifs/search?api_key=' . 'ME169lHlroe4NDoWs6rpe6kM7AyW6n6l' . '&q=' . $query . '&limit=4';
            $response = $client->request('GET', $service_url)->getBody();
            $responseBody = json_decode($response->getContents());
            $responseData = (array) $responseBody->data;
            Cache::put('data', $responseData, now()->addMinutes(1));
            Cache::put('searchTerm', $query, now()->addMinutes(1));
        } catch (ClientException $e) {
            if ($e->hasResponse) {
                dd($e->response);
            }
        }
        return redirect()->route('searchResults')->with(['data' => $responseData, 'searchTerm' => $query]);
    }

    /**
     * show function
     *
     * Gets the data returned from the search and Sends to /searchResults View.
     * 
     * @return view
     */
    public function show()
    {
        if (Cache::has('data')) {
            $data = Cache::get('data');
            $searchTerm = Cache::get('searchTerm');
        } else {
            $data = Session::get('data');
            $searchTerm = Session::get('searchTerm');
        }
        return View('searchResults')->with(array('data' => $data, 'searchTerm' => $searchTerm));
    }
}
