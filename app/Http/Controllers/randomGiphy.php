<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class randomGiphy extends Controller
{

    /**
     * show function
     * 
     * Gets gif from the Database and returns to the view.
     * 
     * @return void
     */
    public function show()
    {
        $model = new \App\GiphyModel();
        $data = $model->getRandomGiphy();
        return View('/randomGiphy')->with(array('gif' => $data));
    }


    /**
     * fetchRandomGiphyData
     * Queries giphy random endpoint to return a random gif.
     * 
     * @return array|$responseData
     */
    public function fetchRandomGiphyData()
    {
        $client = new \GuzzleHttp\Client();
        $service_url = 'http://api.giphy.com/v1/gifs/random?api_key=' . 'ME169lHlroe4NDoWs6rpe6kM7AyW6n6l';
        $response = $client->request('GET', $service_url)->getBody();
        $responseBody = json_decode($response->getContents());
        $responseData = (array) $responseBody->data;

        return $responseData;
    }
}
