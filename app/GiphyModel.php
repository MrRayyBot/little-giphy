<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class GiphyModel extends Model
{
    public function insertRandomGiphyData(string $type, string $url, string $title)
    {
        DB::table('randomGiphys')->insert(['type' => $type], ['url' => $url], ['title' => $title]);
    }

    public function getRandomGiphy()
    {
        return DB::table('randomGiphys')->first();
    }

    public function modifyRandomgiphyRecord()
    { 
        
    }
}
