<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

<body>
    <h1 style="text-align: center;"> Random Giphy: {{ $gif->title }}</h1>
    <a class="text-align: center;" href="/">Back</a>
    <div class="container">
        <div class="row">
            <div class="d-inline-block col-sm-4">
                <p class="text-center text-uppercase">{{ $gif->title }}</p>
                <img src="{{ $gif->url }}" style="height:350px; width:350px;">
            </div>
        </div>
    </div>
</body>
</html>