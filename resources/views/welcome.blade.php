<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

<body>
    <div class="container" style="margin-top: 8%;">
        <div class="col-md-6 col-md-offset-3">
            <div class="row">
                <div id="logo" class="text-center">
                    <h1>Little Giphy</h1>
                    <p>There in a Giphy</p>
                </div>
                <form class="form-inline active-purple-3 active-purple-4" action="/search" method="POST" role="search">
                    {{ csrf_field() }}
                    <div class="form-group" style="margin-left: 27%;">
                        <div class="input-group">
                            <input id="1" class="form-control" type="text" name="search" placeholder="Search..." required/>
                            <span class="input-group-btn">
                <button class="btn btn-primary" type="submit">
                    Search
                </button>
                </span>
                        </div>
                    </div>
                </form>
            </div>
            <form style="margin-left: 43%; margin-top: 10px; margin-bottom: 10px;" class="form-inline active-purple-3 active-purple-4" action="/randomGiphy" method="GET" role="random">
                <button class="btn btn-success" type="submit"> Random </button>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @foreach($response as $data)
            <div class="d-inline-block col-sm-4">
                <p class="text-center text-uppercase">{{ $data->title }}</p>
                <img src="{{ $data->images->original->url }}" style="height:350px; width:350px;">
            </div>
            @endforeach()
        </div>
    </div>
</body>

</html>