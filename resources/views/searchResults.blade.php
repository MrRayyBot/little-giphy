<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

<body>
    <div class="container">
        <div class="row">
            <h1 style="text-align: center;"> Search Results: {{ $searchTerm }}</h1>
            <a style="text-align:center;" href="/">Back</a>
        </div>
        <div class="row">
            @foreach($data as $gifs)
            <div class="d-inline-block col-sm-4">
                <p class="text-center text-uppercase">{{ $gifs->title }}</p>
                <img src="{{ $gifs->images->original->url }}" style="height:350px; width:350px;">
            </div>
            @endforeach()
        </div>
    </div>
</body>
</html>