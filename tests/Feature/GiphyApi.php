<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class GiphyApi extends TestCase
{
    /**
     * A feature text example to confirm the Trending page returns 200 response with json
     *
     * @return void
     */
    public function testTrendingGiphys()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

    }

/**
 * A basic feature test for JSON API.
 * 
 * @return void
 */
    public function testSearchResults()
    {
        $this->withoutMiddleware();
        
        $response = $this->json('GET', '/search');

        $response
            ->assertStatus(200)
            ->assertJson([
                'created' => true,
            ]);
    }

}
