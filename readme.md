# lLittle Giphy

Basic Laravel App For Viewing and Searching Giphys

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Requirements.


Composer.

```
Composer install

```

Laravel 

```
PHP >= 7.2.0
BCMath PHP Extension
Ctype PHP Extension
JSON PHP Extension
Mbstring PHP Extension
OpenSSL PHP Extension
PDO PHP Extension
Tokenizer PHP Extension
XML PHP Extension

```

You can install laravel via the laravel installer.

```
composer global require laravel/installer

```

## Running the tests

You can run the unit test by doing either of the following.

Navigate to project folder in terminal

- vendor/bin/phpunit
 OR
- vendor/bin/phpunit --filter [METHOD_NAME] [CLASS_NAME] [PATH_TO_FILE]

### Break down into end to end tests

Run All unit tests cases.

```
vendor/bin/phpunit

```

### And coding style tests

* Test Trending Gihpys Home Page Returns 200 Response

```
vendor/bin/phpunit --filter testTrendingGiphys tests/Feature/GiphyApi.php

```

## Built With

* [Laravel](https://laravel.com/docs/5.8) - The web framework used
* [composer](https://getcomposer.org/doc/) -The Package Manager
* [Guzzle](http://docs.guzzlephp.org/en/stable/) - The Http Client

## Authors

* **Ramon Benton** - *Initial work*